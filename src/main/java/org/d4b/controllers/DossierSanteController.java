package org.d4b.controllers;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.d4b.dto.DossierSanteDTO;
import org.d4b.models.DossierSante;

import org.d4b.services.DossierSanteService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


import java.util.Optional;

@Slf4j(topic = "DossierSante")
@RestController
@RequestMapping("/dossiers-sante")
@RequiredArgsConstructor
public class DossierSanteController {

    private final DossierSanteService dossierSanteService;
    ModelMapper modelMapper = new ModelMapper();


    @GetMapping
    public Flux<DossierSanteDTO> fetchDossiersSante(Optional<String> start, Optional<String> limit) {

        // convert to DTO
        return dossierSanteService.fetchAll(start, limit)
                .map(ds -> modelMapper.map(ds, DossierSanteDTO.class));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<DossierSanteDTO> persistDossierSante(@RequestBody DossierSanteDTO dossierSanteDTO) {

        DossierSante newDossierSante = modelMapper.map(dossierSanteDTO, DossierSante.class);

        return dossierSanteService.persist(newDossierSante)
                .map(ds -> modelMapper.map(ds, DossierSanteDTO.class));
    }
}
