package org.d4b.services;

import org.d4b.models.DossierSante;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;

public interface DossierSanteService {

    public Flux<DossierSante> fetchAll(Optional<String> start, Optional<String> limit);

    /**
     * Save the "Dossier Sante" Object.
     * @param newDossierSante DossierSante to save
     */
   public Mono<DossierSante> persist(DossierSante newDossierSante);
}
