package org.d4b.services.impl;


import lombok.RequiredArgsConstructor;
import org.d4b.models.DossierSante;
import org.d4b.repository.DossierSanteRepository;
import org.d4b.services.DossierSanteService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DossierSanteServiceImpl implements DossierSanteService {

    private final DossierSanteRepository dossierSanteRepository;

    @Override
    public Flux<DossierSante> fetchAll(Optional<String> start, Optional<String> limit) {
        Flux<DossierSante> ds = dossierSanteRepository.findAll();
        return ds;
    }

    @Override
    public Mono<DossierSante> persist(DossierSante newDossierSante) {
        return dossierSanteRepository.save(newDossierSante);
    }
}
