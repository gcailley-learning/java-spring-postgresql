package org.d4b;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSpringReactivePostgresqlApplication {

	public static void main(String[] args) {

		SpringApplication.run(JavaSpringReactivePostgresqlApplication.class, args);
	}

}
