package org.d4b.builder;

import org.d4b.models.DossierSante;

public class DossierSanteBuilder {
    public static DossierSante create(String description, String assureName, String assurePhone, String assurerEmail) {
        return new DossierSante(null, description, assureName, assurePhone, assurerEmail);
    }
}
