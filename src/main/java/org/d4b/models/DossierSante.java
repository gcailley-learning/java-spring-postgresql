package org.d4b.models;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;


@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of = {"description", "assureName"})
@Data
@Table
public class DossierSante  {
    @Id
    String id;
    String description;
    String assureName;
    String assurePhone;
    String assureEmail;
}
