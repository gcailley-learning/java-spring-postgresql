package org.d4b.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;


@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of = {"description", "assureName"})
public class DossierSanteDTO {

        String id;
        String description;
        String assureName;
        String assurePhone;
        String assureEmail;
}
