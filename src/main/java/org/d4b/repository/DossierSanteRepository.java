package org.d4b.repository;

import org.d4b.models.DossierSante;
import org.springframework.data.r2dbc.repository.R2dbcRepository;

public interface DossierSanteRepository extends R2dbcRepository<DossierSante, String> {

}
