INSERT INTO assure("id", "name", "phone", "email") VALUES (1, 'John Doe', '(408)-111-1234', 'john.doe@bluebird.dev');
INSERT INTO assure("id", "name", "phone", "email") VALUES (2, 'Jane Doe', '(408)-111-1235', 'jane.doe@bluebird.dev');

INSERT INTO dossier_sante ("id", "description", "assure_id") VALUES(nextval('hibernate_sequence'), 'New BlueBird Inc',	1);
INSERT INTO dossier_sante ("id", "description", "assure_id") VALUES(nextval('hibernate_sequence'), 'Dolphin LLC',	2);


