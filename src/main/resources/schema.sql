
DROP TABLE IF EXISTS dossier_sante;

CREATE TABLE dossier_sante(
   id INT GENERATED ALWAYS AS IDENTITY,
   description VARCHAR(255) NOT NULL,
   assure_name VARCHAR(255) NOT NULL,
   assure_phone VARCHAR(15),
   assure_email VARCHAR(100),
   PRIMARY KEY(id)
);

